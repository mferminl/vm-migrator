try:
    from setuptools import find_packages
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name = 'vm-migrator',
    version = '0.0.1',
    description = 'Application to provide VM migrations between hypervisors',
    author = 'Marcos Fermin Lobo',
    author_email = 'marcos.fermin.lobo@cern.ch',
    packages=find_packages(),
    include_package_data = True,
    data_files = [('etc/vm-migrator', 
                  ['etc/vm-migrator/vm-migrator.conf',
                   'etc/vm-migrator/logging.conf'])],
)
