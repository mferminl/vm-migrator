import unittest

from tests.test_base import TestBase

from openstackclient.common import exceptions as openstack_ex
from vmmigrator.exceptions.common import *
from vmmigrator.validate_candidate import *

import mock

class TestVMCandidate(TestBase):

    def test_vm_active(self):
        """VM is in active state (needed by Live Migration)"""
        valcoldmig = ValidateCandidateLiveMigration(self.vm_2.id)
        valcoldmig.server = self.vm_2

        r = valcoldmig.vm_check_status()
        self.assertTrue(r)

    @mock.patch('vmmigrator.validate_candidate.ValidateCandidate.vm_check_status', return_value=True)
    @mock.patch('vmmigrator.step.Step.setup_openstackclient')
    def test_vm_good_candidate(self, mock_setup_openstackclient,
                               mock_vm_check_status):
        """VM is a good candidate for COLD and LIVE migration"""
        valcoldmig = ValidateCandidateLiveMigration(self.vm_2.id)

        with mock.patch('vmmigrator.validate_candidate.ValidateCandidate.get_server_object',
                        return_value=self.vm_2):
            valcoldmig.execute()

        mock_setup_openstackclient.assert_called_with()
        mock_vm_check_status.assert_called_with()

        valcoldmig = ValidateCandidateColdMigration(self.vm_1.id)

        with mock.patch('vmmigrator.validate_candidate.ValidateCandidate.get_server_object',
                        return_value=self.vm_1):
            valcoldmig.execute()

        mock_setup_openstackclient.assert_called_with()
        mock_vm_check_status.assert_called_with()

    @mock.patch('vmmigrator.validate_candidate.ValidateCandidate.vm_check_status', return_value=False)
    @mock.patch('vmmigrator.step.Step.setup_openstackclient')
    def test_vm_bad_candidate_status(self, mock_setup_openstackclient,
                                     mock_vm_check_status):
        """VM is NOT a good candidate due too wrong status for kind migration"""
        valcoldmig = ValidateCandidateLiveMigration(self.vm_2.id)

        with mock.patch('vmmigrator.validate_candidate.ValidateCandidate.get_server_object',
                        return_value=self.vm_2):
            try:
                valcoldmig.execute()
            except CandidateBadStatus:
                self.assertTrue(1)

        mock_setup_openstackclient.assert_called_with()
        mock_vm_check_status.assert_called_with()

    @mock.patch('vmmigrator.validate_candidate.ValidateCandidate.get_server_object')
    @mock.patch('vmmigrator.validate_candidate.ValidateCandidate.vm_check_status', return_value=True)
    @mock.patch('vmmigrator.step.Step.setup_openstackclient')
    def test_vm_bad_candidate_server_not_found(self,
                                               mock_setup_openstackclient,
                                               mock_vm_check_status,
                                               mock_get_server_object):
        """System raises exception when the the server candidate is
        not found"""
        mock_get_server_object.side_effect = openstack_ex.NotFound("Not found")
        valcoldmig = ValidateCandidateLiveMigration(self.vm_2.id)

        try:
            valcoldmig.execute()
        except openstack_ex.NotFound:
            self.assertTrue(1)

        mock_setup_openstackclient.assert_called_with()
        self.assertFalse(mock_vm_check_status.called)

    @mock.patch('vmmigrator.validate_candidate.ValidateCandidate.get_server_object')
    @mock.patch('vmmigrator.validate_candidate.ValidateCandidate.vm_check_status', return_value=True)
    @mock.patch('vmmigrator.step.Step.setup_openstackclient')
    def test_vm_bad_candidate_volume_attached(self,
                                               mock_setup_openstackclient,
                                               mock_vm_check_status,
                                               mock_get_server_object):
        """System raises exception when server has volume/s
        attached during a Live Migration"""
        valcoldmig = ValidateCandidateLiveMigration(self.vm_2.id)

        try:
            valcoldmig.execute()
        except Exception:
            self.assertTrue(1)

        mock_setup_openstackclient.assert_called_with()
        mock_get_server_object.assert_called_with()
        mock_vm_check_status.assert_called_with()

    def test_vm_is_shutoff(self):
        """VM is in shutoff state (needed by Cold Migration)"""
        valcoldmig = ValidateCandidateColdMigration(self.vm_1.id)
        valcoldmig.server = self.vm_1

        r = valcoldmig.vm_check_status()
        self.assertTrue(r)

    def test_vm_shutoff_if_not(self):
        """Shutoff VM if is not already """
        pass

    def test_vm_has_volumes_attached(self):
        """Vm has/not volumes attached in COLD Migration"""
        pass

    def test_vm_is_too_bussy(self):
        """Check if the VM is too busy to be migrated"""
        pass

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestVMCandidate)
    unittest.TextTestRunner(verbosity=2).run(suite)
