import unittest

from tests.test_base import TestBase

from vmmigrator.migrator import Migrator
from vmmigrator.openstack_client_factory import OpenstackClientFactory

import mock


"""
Suite test for Authencation. This test executes real conections
"""
class TestRealAuth(TestBase):
    def test_get_cloud_client(self):
        """Get the cloud client authenticated from cci-tools module
        """
        cloud = OpenstackClientFactory().get_cloudclient()
        self.assertNotEqual(cloud, None)

    def test_migrator_auth(self):
        """Test the authentication method in Migrator class
        """
        m = Migrator(self.hyper_1.name)
        m.authenticate()

        self.assertNotEqual(m.cloud, None)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRealAuth)
    unittest.TextTestRunner(verbosity=2).run(suite)
