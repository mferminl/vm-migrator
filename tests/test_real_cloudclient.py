import unittest

from novaclient.exceptions import ClientException
from novaclient.v1_1.hypervisors import Hypervisor

from tests.test_base import TestBase

from vmmigrator.migrator import Migrator

import mock

class TestRealCloudclient(TestBase):
    """Test suite to check all the methods which call to some
    cloud client method. This tests are executed with real conections
    and authenticated.
    """

    """Migrator object"""
    m = None

    """These are real hypervisors to be migrated"""
    source = ['p01001464006125', 'p01001464047091']

    def setUp(self):
        self.m = Migrator(self.source)
        self.m.authenticate()

    def test_get_hypervisor(self):
        """(REAL) Get hypervisor information
        """
        hyper = self.m._get_hypervisor(self.source[0])
        self.assertIsInstance(hyper, Hypervisor)

    def test_get_servers_by_hypervisor(self):
        """(REAL) Get servers from hypervisor
        """
        servers = self.m._get_servers_by_hypervisor(self.source[0])

        self.assertTrue(len(servers) > 0)


    def test_get_servers_by_hypervisor_not_found(self):
        """(REAL) Get servers from hypervisor which does not exist
        """
        try:
            servers = self.m._get_servers_by_hypervisor('pxxxxmferminlxxxx')
            self.assertTrue(0)
        except ClientException:
            self.assertTrue(1)

    def test_hypervisor_not_found(self):
        """(REAL) Try to get information from a not found hypervisor
        """
        try:
            hyper = self.m._get_hypervisor('pxxxxmferminlxxxx')
            self.assertTrue(0)
        except ClientException:
            self.assertTrue(1)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRealCloudclient)
    unittest.TextTestRunner(verbosity=2).run(suite)
