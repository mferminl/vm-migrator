import unittest

from tests.test_base import TestBase

from vmmigrator.migrator import Migrator

import mock

class TestGetServersHypervisor(TestBase):
    """Suite test for get Servers step depending of the input parameters
    """

    def test_get_servers_by_hypervisor(self):
        """Check if get all servers from hypervisor is executed without --vms
        parameter"""
        servers_exp = [self.vm_1, self.vm_2]

        m = Migrator([self.hyper_1.name])
        self.assertTrue(len(m.vms) == 0)

        with mock.patch('vmmigrator.migrator.Migrator._get_servers_by_hypervisor',
                        return_value=servers_exp):
            servers = m.get_servers_migrate()
            self.assertTrue(len(servers) > 0)
            self.assertTrue(len(servers) == len(servers_exp))
            self.assertEqual(servers[0].name, servers_exp[0].name)
            self.assertEqual(servers[1].name, servers_exp[1].name)

    def test_get_specific_servers(self):
        """Check if get only specific servers from hypervisor is executed with --vms
        parameter"""
        servers_in_hyper = [self.vm_1, self.vm_2]
        servers_exp = [self.vm_1]

        m = Migrator([self.hyper_1.name], vms=[servers_exp[0].name])
        self.assertTrue(len(m.vms) == 1)

        with mock.patch('vmmigrator.migrator.Migrator._get_servers_by_hypervisor',
                        return_value=servers_in_hyper):
            servers = m.get_servers_migrate()
            self.assertTrue(len(servers) > 0)
            self.assertTrue(len(servers) == len(servers_exp))
            self.assertEqual(servers[0].name, servers_exp[0].name)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestGetServersHypervisor)
    unittest.TextTestRunner(verbosity=2).run(suite)
