import logging
import unittest

from tests import default_fixtures

# Disable logging during all tests by default
logging.disable(logging.ERROR)

class TestBase(unittest.TestCase):

    def setUp(self):
        super(TestBase, self).setUp()
        self.load_fixtures(default_fixtures)

    def load_fixtures(self, fixtures):
        self.load_fixture_list('vm', fixtures.VMS)
        self.load_fixture_list('hyper', fixtures.HYPERVISORS)
        self.load_fixture_list('flavor', fixtures.FLAVORS)

    def load_fixture_list(self, prefix, fix):
        for dict_fix in fix:
            attrname = '{0}_{1}'.format(prefix, dict_fix['id'])
            setattr(self, attrname, self.load_object(dict_fix, Object()))

    def load_object(self, dict, o):
        for att, val in dict.items():
            setattr(o, att, val)
        return o

class Object(object):
    pass
