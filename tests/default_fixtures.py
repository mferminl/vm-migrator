VMS = [
    {
        'id': 1,
        'name': 'vm1',
        'status': 'SHUTOFF',
       'os-extended-volumes:volumes_attached': [{'id': '0000000000000000'}]
    },
    {
        'id': 2,
        'name': 'vm2',
        'status': 'ACTIVE',
        'os-extended-volumes:volumes_attached': []
    },
    {
        'id': 3,
        'name': 'vm3',
        'status': 'ACTIVE',
        'os-extended-volumes:volumes_attached': [],
        'config_drive': True
    }
]

HYPERVISORS = [
    {
        'id':'1',
        'name': 'p0000000001'
    },
    {
        'id':'2',
        'name': 'p0000000002'
    }

]

FLAVORS = [
{
    'name': 'm1.tiny',
    'links': [{
        'href': 'http://openstack.:8774/v2/71f66236-1163-42c2-b808-f6f3442683ee/flavors/1', 'rel': 'self'
    }, {
        'href': 'http://openstack.:8774/71f66236-1163-42c2-b808-f6f3442683ee/flavors/1', 'rel': 'bookmark'
    }],
    'ram': 512,
    'vcpus': 1,
    'id': '1',
    'OS-FLV-DISABLED:disabled': False,
    'swap': '',
    'os-flavor-access:is_public': True,
    'rxtx_factor': 1.0,
    '_info': {
        'name': 'm1.tiny', 'links': [{
            'href': 'http://openstack.:8774/v2/71f66236-1163-42c2-b808-f6f3442683ee/flavors/1', 'rel': 'self'
        }, {
            'href': 'http://openstack.:8774/71f66236-1163-42c2-b808-f6f3442683ee/flavors/1', 'rel': 'bookmark'
        }], 'ram': 512, 'OS-FLV-DISABLED:disabled': False, 'vcpus': 1, 'swap': '', 'os-flavor-access:is_public': True, 'rxtx_factor': 1.0, 'OS-FLV-EXT-DATA:ephemeral': 0, 'disk': 0, 'id': '1'
    },
    'disk': 0,
    'OS-FLV-EXT-DATA:ephemeral': 0,
    '_loaded': True
}
]
