import unittest

from tests.test_base import TestBase

from vmmigrator.config_factory import ConfigFactory

import mock

"""
Suite test for config file values
"""
class TestConfigFile(TestBase):
    def test_all_vbles_set(self):
        """Check all the config vars in the config file and configured
        """
        cf = ConfigFactory()

        self.assertEqual("AUTH", cf.section)

        options = cf.get_options()
        self.assertTrue(len(options) == 5)

        self._check_config_vars(cf.config, options, cf.section)

    def _check_config_vars(self, config, options, section):
        self.assertTrue('os_auth_url' in options)
        self.assertEqual(config.get(section, 'os_auth_url'), 'https://keystone.cern.ch/krb/v3')
        self.assertTrue('os_identity_api_version' in options)
        self.assertEqual(config.get(section, 'os_identity_api_version'), '3')
        self.assertTrue('os_project_domain_id' in options)
        self.assertEqual(config.get(section, 'os_project_domain_id'), 'default')
        self.assertTrue('os_tenant_name' in options)
        self.assertEqual(config.get(section, 'os_tenant_name'), 'admin')
        self.assertTrue('os_auth_type' in options)
        self.assertEqual(config.get(section, 'os_auth_type'), 'v3kerberos')

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestConfigFile)
    unittest.TextTestRunner(verbosity=2).run(suite)
