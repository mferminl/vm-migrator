import unittest

from tests.test_base import TestBase

from vmmigrator.migrator import Migrator

import mock

class TestHypervisorTarget(TestBase):

    def test_target_param(self):
        """Test if the target hypervisor param is set when is passed
        """
        m = Migrator(self.hyper_1.name, target=self.hyper_2.name)

        self.assertEqual(self.hyper_2.name, m.target)


    def test_hyper_is_reachable(self):
        """Check if the target hypervisor exists in the system
        """
        m = Migrator(self.hyper_1.name, target=self.hyper_2.name)

        with mock.patch('vmmigrator.migrator.Migrator._get_hypervisor',
                        return_value=self.hyper_2):
            h = m.get_target_hypervisor()

        self.assertEqual(self.hyper_2.name, h.name)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestHypervisorTarget)
    unittest.TextTestRunner(verbosity=2).run(suite)
