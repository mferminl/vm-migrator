import unittest

from tests.test_base import TestBase

from vmmigrator.migrator import Migrator

import mock

class TestMigrator(TestBase):
    """Suite test for migration workflow
    """

    @mock.patch('vmmigrator.migrator.Migrator.categorize_servers')
    @mock.patch('vmmigrator.migrator.Migrator.authenticate')
    def test_run(self, mock_authenticate, mock_categorize_servers):
        """Check if proper methods are called in the migration process"""
        m = Migrator(self.hyper_1.name)
        server_list = [self.vm_1, self.vm_2]

        with mock.patch('vmmigrator.migrator.Migrator._get_all_servers',
                        return_value=server_list):
            m.run()

        # check authtentication call
        mock_authenticate.assert_called_with()
        # Categorize servers. It'l remove the servers in category E (see OS-2440)
        mock_categorize_servers.assert_called_with()
        # Check if the category matches with the server inside
        # validate them
        # Call actions pre-migrate
        # Call migrate for each server
        # Check the status of each server migrated

    def test_categorize_servers(self):
        """Return only servers without 'config drive' (category E)"""
        m = Migrator(self.hyper_1.name)
        m.server_list_raw = [self.vm_1, self.vm_2, self.vm_3]
        server_list_exp = [self.vm_1, self.vm_2]

        server_migrate_list = m.categorize_servers()

        self.assertIsInstance(server_migrate_list, list)
        self.assertTrue(len(server_migrate_list) == 2)
        self.assertEqual(server_migrate_list[0].server.id, self.vm_1.id)
        self.assertEqual(server_migrate_list[1].server.id, self.vm_2.id)

    def test_categorize_servers_empty_raw_list(self):
        """Launch exception trying to categorize servers from a empty list"""
        m = Migrator(self.hyper_1.name)

        with self.assertRaises(ValueError):
            l = m.categorize_servers()

    @mock.patch('vmmigrator.migrator.Migrator._get_all_servers', return_value=[])
    def test_get_servers_migrate_emtpy_list(self, mock_get_all_servers):
        """Launch exception when get_servers_migrate will return empty server
        list"""
        m = Migrator(self.hyper_1.name)

        with self.assertRaises(ValueError):
            l = m.get_servers_migrate()

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestMigrator)
    unittest.TextTestRunner(verbosity=2).run(suite)
