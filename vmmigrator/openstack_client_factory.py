import logging
import os

from ccitools.cloud import CloudClient

from vmmigrator.config_factory import ConfigFactory

logger = logging.getLogger(__name__)


"""
Class to instanciate OpenStack Client object
"""
class OpenstackClientFactory(object):

    def get_cloudclient(self):
        logger.info("Exporting environment vars...")
        self.export_auth_vars()

        logger.info("Instanciating cloudclient object...")
        cloud = CloudClient()
        logger.info("cloudclient object instanciated")

        return cloud

    def export_auth_vars(self):
        cf = ConfigFactory()
        options = cf.get_options()
        for option in options:
            os.environ[option.upper()] = cf.config.get(cf.section, option)

