import argparse
import logging
import logging.config
import os

from ConfigParser import ConfigParser

from openstackclient.common import exceptions as openstack_ex

from vmmigrator.exceptions.common import CandidateBadStatus
from openstack_client_factory import *
from validate_candidate import *

logger = logging.getLogger(__name__)
config_file = 'etc/vm-migrator/vm-migrator.conf'

def get_args():
    parser = argparse.ArgumentParser()
    # Mandatory args
    parser.add_argument("source",
                        metavar="source",
                        nargs='+',
                        help="Hypervisor name list from the VMs (all) will " \
                             "be migrated to")
    #optional args
    parser.add_argument("--debug",
                        help="Activate DEBUG mode",
                        action="store_true")
    parser.add_argument("--vms",
                        nargs='+',
                        metavar="server",
                        help="Specifies a list of server names to migrate")
    parser.add_argument("--target",
                        metavar="hypervisor",
                        help="Name of the hypervisor where the VMs will be " \
                             "placed to")

    return parser.parse_args()

def export_auth_vars(config):
    section = 'AUTH'
    options = config.options(section)
    for option in options:
        #globals()[option] = config.get(section, option)
        os.environ[option.upper()] = config.get(section, option)

def get_list_steps(args, oc):
    # Set the arguments
    mode = args.mode
    source_hypervisor_name = args.source
    server_id = args.vms
    target_hypervisor_name = args.target

    if mode == 'live':
        step1 = ValidateCandidateLiveMigration(server_id, oc)
    else:
        step1 = ValidateCandidateColdMigration(server_id, oc)

    return [step1]

def main():
    # Get command arguments
    args = get_args()
    if args.debug:
        logger.setLevel(logging.DEBUG)
    print args
    return 1
    print "=================================================================="
    print "=================== Not implemented yet (DRYRUN) ================="
    print "=================================================================="

    config = ConfigParser()
    config.readfp(open(config_file))

    # Set auth environment vars
    export_auth_vars(config)

    # Get OpenStack Client
    oc = OpenstackClientFactory().get_openstackclient()

    logger.info('Starting migration...')

    # Get step list
    step_list = get_list_steps(args, oc)

    try:
        for s in step_list:
            s.execute()
        logger.info("Migration end.")
    except CandidateBadStatus as e:
        logger.error(e.message)
        logger.info("Migration cancelled")
    except openstack_ex.NotFound as e:
        logger.error("NotFound exception")
        logger.error(e.message)
    except Exception as e:
        logger.error("General exception: {0}".format(e.message))

if __name__ == "__main__":
    main()
