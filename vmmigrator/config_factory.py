from ConfigParser import ConfigParser

config_file = 'etc/vm-migrator/vm-migrator.conf'

"""Class to manage configuration file
"""
class ConfigFactory(object):
    section = ''
    config = None

    def __init__(self):
        self.section = 'AUTH'
        self.config = ConfigParser()
        self.config.readfp(open(config_file))

    def get_options(self):
        """Returns a list (strings) for config vars in the section. No values.
        :returns: list(<string>)
        """
        return self.config.options(self.section)
