import logging

from vmmigrator.exceptions.common import CandidateBadStatus
from step import Step

logger = logging.getLogger(__name__)

class ValidateCandidate(Step):

    valid_status = ''

    server_id = ''
    server = None
    server_has_volumes_attached = False

    migration_type = ''

    def __init__(self, server_id, oc):
        super(ValidateCandidate, self).__init__(oc)
        self.server_id = server_id

    def command(self):
        super(ValidateCandidate, self).command()
        logger.info("Checking if Server is a good candidate...")

        # Check Server status
        self.step_vm_check_status()

        # Check if server has volumes attached
        self.step_vm_check_volumes_attached()

        logger.info("Good candidate!")

    def get_server_object(self):
        """Returns a server object from openstack"""
        return self.openstack.compute.servers.get(self.server_id)

    def get_ids_volumes_attached(self):
        return getattr(self.server, 'os-extended-volumes:volumes_attached')

    def pre_execute(self):
        super(ValidateCandidate, self).pre_execute()
        logger.info("Get server data")
        self.server = self.get_server_object()
        logger.info("Server '{0}' ({1}) will be " \
                    "migrated".format(self.server.name, self.server.id))

    def step_vm_check_status(self):
        logger.info("Checking the right status ({0}) of the Server for '{1}' " \
                    "migration...".format(self.valid_status,
                                          self.migration_type))
        if not self.vm_check_status():
            logger.error("Bad candidate!. Wrong status for this kind of " \
                         "migration")
            raise CandidateBadStatus(server_id=self.server_id,
                                     status=self.server.status,
                                     exp_status=self.valid_status)
        logger.info("Server '{0}' is in good status for '{1}' " \
                    "migration".format(self.server_id,
                                       self.migration_type))

    def step_vm_check_volumes_attached(self):
        logger.info("Checking if Server has volumes attached...")
        self.server_has_volumes_attached = self.vm_check_volumes_attached()
        logger.info("Server '{0}' with volumes attached: " \
                    "{1}".format(self.server.id,
                                 self.server_has_volumes_attached))

    def vm_check_status(self):
        return self.server.status == self.valid_status

    def vm_check_volumes_attached(self):
        pass

class ValidateCandidateLiveMigration(ValidateCandidate):
    migration_type = 'LIVE'
    valid_status = 'ACTIVE'

    def vm_check_volumes_attached(self):
        vols = self.get_ids_volumes_attached()
        if vols:
            vols_id = []
            for v in vols:
                vols_id.append(v['id'])
            raise Exception("Server '{0}' has '{1}' volumes attached ({2})." \
                            " LIVE migration is not " \
                            "possible".format(self.server.id, len(vols),
                                              vols_id))

class ValidateCandidateColdMigration(ValidateCandidate):
    migration_type = 'COLD'
    valid_status = 'SHUTOFF'

    def vm_check_volumes_attached(self):
        return (self.get_ids_volumes_attached())

