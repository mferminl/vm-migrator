class CandidateBadStatus (Exception):
    base_msg = "VM '{0}' has '{1}' status and should have '{2}' status"
    def __init__(self, server_id, status, exp_status):
        self.base_msg = self.base_msg.format(server_id, status, exp_status)
        super(CandidateBadStatus, self).__init__(self.base_msg)
