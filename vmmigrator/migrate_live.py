from vmmigrator.migrate_mode import MigrateMode

class MigrateLive(MigrateMode):

    def __init__(self, server):
        super(MigrateLive, self).__init__(server)
        self.validator = None
