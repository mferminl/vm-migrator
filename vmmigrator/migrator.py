from vmmigrator.migrate_cold import MigrateCold
from vmmigrator.migrate_live import MigrateLive

from vmmigrator.openstack_client_factory import OpenstackClientFactory

class Migrator(object):
    """Main class to execute the migration
    """

    """Hypersvisor name list from the servers will be migrated"""
    source = []

    """List of server names to be migrated. If this parameter is
    set, only these servers will be migrated from the source hypervisor.
    """
    vms = []

    """Hypervisor name where the servers will be migrated
    """
    target = ''

    """Object to manage Cloud client from cci-tools module"""
    cloud = None

    """Server object list to be migrated and not filtered (not categorized)"""
    server_list_raw = []

    """MigrateMode object list, with servers not validated, to be migrated"""
    server_migrate_not_checked_list = []

    """MigrateMode object list (final) to be migrated""" 
    server_migrate_list = []

    def __init__(self, source, vms=[], target=''):
        """Constructor
        :param source: (list<string>) Hypersvisor name from the servers will
                       be migrated
        :param vms: list(<string>) (Optional) List of server names to be
                    migrated
        :param target: (string) (Optional) Hypervisor name where the servers
                       will be migrated
        """
        self.source = source
        self.vms = vms
        self.target = target

    def _get_all_servers(self):
        """Returns a server list from all the source hypervisors
        :returns: list(<Server>)
        """
        servers = []
        for hyper in self.source:
            servers = servers + self._get_servers_by_hypervisor(hyper)
        return servers

    def _get_hypervisor(self, hyper_name):
        """Ask for hypervisor information to Compute component in cloud client
        :returns: Hypervisor object
        """
        return self.cloud.get_hypervisor_by_name(hyper_name)

    def _get_servers_by_hypervisor(self, hyper_name):
        """Ask to OpenStack client to return the full server list from the source
        hypervisor.
        :returns: list(<Server>)
        """
        return self.cloud.get_servers_by_hypervisor(hyper_name)

    def _get_specific_servers(self):
        """Returns a list of specific servers, which names are set in "vms"
        attribute, from the source list of hypervisors
        :returns: list(<Server>)
        """
        servers = self._get_all_servers()
        servers_ret = []
        for obj_server in servers:
            if obj_server.name in self.vms:
                servers_ret.append(obj_server)
        return servers_ret

    def _get_ids_volumes_attached(self, server):
        """Returns a volume list from Server object, in case it has
        :returns: list(<Volume>)
        """
        return getattr(server, 'os-extended-volumes:volumes_attached')

    def authenticate(self):
        """Set the cloud client from cci-tools module
        """
        self.cloud = OpenstackClientFactory().get_cloudclient()

    def categorize_servers(self):
        """From self.server_list_raw, remove all servers with 'config drive'
        configured (category E, see OS-2440)
        """
        if len(self.server_list_raw) == 0:
            msg = "There is no server list to categorize"
            #loger.error(msg)
            raise ValueError("There is no server list to categorize")
        migrate_mode_list = []
        for server in self.server_list_raw:
            # Servers with 'config_file' configured will not be migrated
            config_drive = getattr(server, 'config_drive', False)
            if not config_drive:
                migrate_mode_obj = None
                if self._get_ids_volumes_attached(server):
                    # cold migration
                    migrate_mode_obj = MigrateCold(server)
                else:
                    # Live Migration
                    migrate_mode_obj = MigrateLive(server)
                migrate_mode_list.append(migrate_mode_obj)
        return migrate_mode_list

    def run(self):
        """Execute the main workflow for migration servers
        """
        # Authenticate
        self.authenticate()
        # Get raw server list, not categorized, to be migrated
        self.server_list_raw = self.get_servers_migrate()
        # Get MigrateMode object list, with servers atached, to be migrated
        self.server_not_checked_migrate_list = self.categorize_servers()
        # Validate the server list and remove invalids
        self.server_migrate_list = self.validate_servers()
        # Execute actions pre-migration in all final servers
        self.pre_migration()
        # EXECUTE MIGRATIONS for self.server_migrate_list
        self.migrate()
        # Execute actions post-migration to check the correct execution
        self.post_migration()

    def get_servers_migrate(self):
        """Returns the server list (list of <Server> objects) to be migrated.
        It depends if the "vms" parameter was set. If "vms" is set, only those
        servers will be migrated. Otherwise, all servers in the source
        hypervisor list will be.
        :returns: list(<Server>)
        """
        server_list = []
        if self.vms:
            server_list = self._get_specific_servers()
        else:
            server_list = self._get_all_servers()
        # Raise exception in case empty list
        if len(server_list) == 0:
            msg = "Servers not found in hypervisor/s " \
                  "'{0}'".format(self.source)
            #logger.error(msg)
            raise ValueError(msg)
        return server_list

    def get_target_hypervisor(self):
        """Returns the target hypervisor information
        :returns: Hypervisor object
        """
        return self._get_hypervisor(self.target)

    def migrate(self):
        pass

    def pre_migration(self):
        pass

    def post_migration(self):
        pass

    def validate_servers(self):
        pass
