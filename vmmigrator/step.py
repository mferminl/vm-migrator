import logging

logger = logging.getLogger(__name__)

"""General class to define a STEP
"""
class Step(object):

    openstack = None

    def __init__(self, openstack_client):
        self.openstack = openstack_client

    def execute(self):
        self.pre_execute()
        self.command()
        self.post_execute()

    def command(self):
        """The actions to be done in the step
        Function to redefine in child classes
        """
        logger.info("COMMAND tasks running...")

    def post_execute(self):
        logger.info("POST COMMAND tasks running...")

    def pre_execute(self):
        logger.info("PRE COMMAND tasks running...")
