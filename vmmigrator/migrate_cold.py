from vmmigrator.migrate_mode import MigrateMode

class MigrateCold(MigrateMode):

    def __init__(self, server):
        super(MigrateCold, self).__init__(server)
        self.validator = None
