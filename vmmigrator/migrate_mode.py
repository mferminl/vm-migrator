class MigrateMode(object):
    """Server object from Nova world"""
    server = None

    """Validator object to check if the Server is in proper state to be
    migrated"""
    validator = None

    """Boolean flag. True, Server can be migrated, False in other case"""
    valid = False

    """Boolean flag. True, Server was migrated, False in other case"""
    migrated = False

    def __init__(self, server):
        self.server = server

    def migrate(self):
        """This method execute the proper nova API call to migrate the
        server. To be implemented by child classes"""
        pass

    def post_migrate(self):
        """Actions to be exeuted after migration. To be implemented by child
        classes if it is necessary"""
        pass

    def pre_migrate(self):
        """Actions to be executed before the migration. To be implemented by
        child classes if it is necessary"""
        pass

    def validate(self):
        """Check the Server parameters to set if is possible to be migrated.
        To be implemented by child classes if it is necessary"""
        return False
