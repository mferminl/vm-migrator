# VM-MIGRATION-APP

## Run
You need a valid kerberos ticket and then:
```
export OS_AUTH_URL=https://keystone.cern.ch/krb/v3
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_DOMAIN_ID=default
export OS_TENANT_NAME='xxxxxxxxxxxxx'
export OS_AUTH_TYPE=v3kerberos

export VM_NAME='example.cern.ch'
export TARGET_HYPERVISOR_NAME='p000000000000'

python vm_mig_app/run.py $VM_NAME $TARGET_HYPERVISOR_NAME
```
